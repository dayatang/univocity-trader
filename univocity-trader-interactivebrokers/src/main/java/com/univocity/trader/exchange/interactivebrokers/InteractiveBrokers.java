package com.univocity.trader.exchange.interactivebrokers;

import com.univocity.trader.*;
import com.univocity.trader.candles.*;
import com.univocity.trader.simulation.*;

import java.util.*;

/**
 * @author uniVocity Software Pty Ltd - <a href="mailto:dev@univocity.com">dev@univocity.com</a>
 */
public final class InteractiveBrokers implements EntryPoint {

	public static final class IBConfiguration extends com.univocity.trader.config.Configuration<IBConfiguration, IBAccountConfiguration> {
		private IBConfiguration() {
			super("ib.properties");
		}

		@Override
		protected IBAccountConfiguration newAccountConfiguration(String id) {
			return new IBAccountConfiguration(id);
		}
	}

	public static final class IBSimulator extends MarketSimulator<IBConfiguration, IBAccountConfiguration> {
		private IBSimulator() {
			super(new IBConfiguration(), IBExchange::new);
		}

		@Override
		protected void backfillHistory(Exchange<?, IBAccountConfiguration> exchange, Collection<String> symbols) {
			for (IBAccountConfiguration account : configure().accounts()) {
				exchange.connectToAccount(account);
			}
			super.backfillHistory(exchange, symbols);
		}
	}

	public static final class IBTrader extends LiveTrader<Candle, IBConfiguration, IBAccountConfiguration> {
		private IBTrader() {
			super(new IBExchange(), new IBConfiguration()
					.updateHistoryBeforeLiveTrading(false)
					.pollCandles(false)
			);
		}
	}

	public static IBSimulator simulator() {
		return new IBSimulator();
	}

	public static IBTrader trader() {
		return new IBTrader();
	}
}
