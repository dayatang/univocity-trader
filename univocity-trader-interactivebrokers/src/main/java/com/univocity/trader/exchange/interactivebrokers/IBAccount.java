package com.univocity.trader.exchange.interactivebrokers;

import com.univocity.trader.*;
import com.univocity.trader.account.*;
import org.slf4j.*;

import java.util.concurrent.*;


class IBAccount implements ClientAccount {

	private static final Logger log = LoggerFactory.getLogger(IBAccount.class);

	private final IBExchange ib;
	private final IBAccountConfiguration account;

	private final ConcurrentHashMap<String, Balance> balances = new ConcurrentHashMap<>();
	private final ConcurrentHashMap<String, OrderBook> orderBooks = new ConcurrentHashMap<>();

	public IBAccount(IBExchange ib, IBAccountConfiguration account) {
		this.ib = ib;
		this.account = account;
		ib.getAccountBalances(account.referenceCurrency(), balances);
	}

	@Override
	public Order executeOrder(OrderRequest orderDetails) {
		return null;
	}

	@Override
	public ConcurrentMap<String, Balance> updateBalances(boolean force) {
		return balances;
	}

	public void resetBalances() {
		balances.clear();
		ib.resetAccountBalances();
	}

	@Override
	public OrderBook getOrderBook(String symbol, int depth) {
		OrderBook book = orderBooks.computeIfAbsent(symbol, s -> new OrderBook(this, symbol, depth));
		ib.getOrderBook(book, false, symbol, depth);
		return book;
	}

	public void closeOrderBook(String symbol) {
		ib.closeOrderBook(symbol);
	}

	@Override
	public Order updateOrderStatus(Order order) {
		return null;
	}

	@Override
	public void cancel(Order order) {

	}
}
