package com.univocity.trader.exchange.interactivebrokers.api;

import com.ib.client.*;
import org.slf4j.*;

import java.util.*;

/**
 *  {@link EWrapper} implementation of methods that are simply logged and are currently not used/ignored.
 *
 * @author uniVocity Software Pty Ltd - <a href="mailto:dev@univocity.com">dev@univocity.com</a>
 */
abstract class IgnoredResponseProcessor implements EWrapper {

	private static final Logger logger = LoggerFactory.getLogger(IgnoredResponseProcessor.class);

	@Override
	public final void marketRule(int marketRuleId, PriceIncrement[] priceIncrements) {
		logger.info(EWrapperMsgGenerator.marketRule(marketRuleId, priceIncrements));
		//TODO: populate symbol information here
	}


	@Override
	public final void historicalDataUpdate(int reqId, Bar bar) {
		historicalData(reqId, bar);
	}

	@Override
	public final void rerouteMktDataReq(int reqId, int conId, String exchange) {
		logger.info(EWrapperMsgGenerator.rerouteMktDataReq(reqId, conId, exchange));
	}

	@Override
	public final void rerouteMktDepthReq(int reqId, int conId, String exchange) {
		logger.info(EWrapperMsgGenerator.rerouteMktDepthReq(reqId, conId, exchange));
	}


	@Override
	public final void pnl(int reqId, double dailyPnL, double unrealizedPnL, double realizedPnL) {
		logger.info(EWrapperMsgGenerator.pnl(reqId, dailyPnL, unrealizedPnL, realizedPnL));
	}

	@Override
	public final void pnlSingle(int reqId, int pos, double dailyPnL, double unrealizedPnL, double realizedPnL, double value) {
		logger.info(EWrapperMsgGenerator.pnlSingle(reqId, pos, dailyPnL, unrealizedPnL, realizedPnL, value));
	}

	@Override
	public final void tickByTickAllLast(int reqId, int tickType, long time, double price, int size, TickAttribLast tickAttribLast, String exchange, String specialConditions) {
		logger.info(EWrapperMsgGenerator.tickByTickAllLast(reqId, tickType, time, price, size, tickAttribLast, exchange, specialConditions));
	}

	@Override
	public final void tickByTickBidAsk(int reqId, long time, double bidPrice, double askPrice, int bidSize, int askSize, TickAttribBidAsk tickAttribBidAsk) {
		logger.info(EWrapperMsgGenerator.tickByTickBidAsk(reqId, time, bidPrice, askPrice, bidSize, askSize, tickAttribBidAsk));
	}

	@Override
	public final void orderBound(long orderId, int apiClientId, int apiOrderId) {
		logger.info(EWrapperMsgGenerator.orderBound(orderId, apiClientId, apiOrderId));
	}

	@Override
	public final void completedOrder(Contract contract, Order order, OrderState orderState) {
		logger.info(EWrapperMsgGenerator.completedOrder(contract, order, orderState));
	}

	@Override
	public final void completedOrdersEnd() {
		logger.info(EWrapperMsgGenerator.completedOrdersEnd());
	}

	@Override
	public final void newsProviders(NewsProvider[] newsProviders) {
		logger.info("Got {} news providers", newsProviders.length);
	}

	@Override
	public final void newsArticle(int i, int i1, String s) {
		logger.info("News article {} {}: {}", i, i1, s);
	}

	@Override
	public final void securityDefinitionOptionalParameter(int reqId, String exchange, int underlyingConId, String tradingClass, String multiplier, Set<String> expirations, Set<Double> strikes) {
		logger.info(EWrapperMsgGenerator.securityDefinitionOptionalParameter(reqId, exchange, underlyingConId, tradingClass, multiplier, expirations, strikes));
	}

	@Override
	public final void securityDefinitionOptionalParameterEnd(int reqId) {
	}

	@Override
	public final void softDollarTiers(int reqId, SoftDollarTier[] tiers) {
		logger.info(EWrapperMsgGenerator.softDollarTiers(tiers));
	}

	@Override
	public final void familyCodes(FamilyCode[] familyCodes) {
		logger.info(EWrapperMsgGenerator.familyCodes(familyCodes));
	}

	@Override
	public final void symbolSamples(int reqId, ContractDescription[] contractDescriptions) {
		logger.info(EWrapperMsgGenerator.symbolSamples(reqId, contractDescriptions));
	}

	@Override
	public final void mktDepthExchanges(DepthMktDataDescription[] depthMktDataDescriptions) {
		logger.info(EWrapperMsgGenerator.mktDepthExchanges(depthMktDataDescriptions));
	}

	@Override
	public final void tickNews(int tickerId, long timeStamp, String providerCode, String articleId, String headline, String extraData) {
		logger.info(EWrapperMsgGenerator.tickNews(tickerId, timeStamp, providerCode, articleId, headline, extraData));
	}

	@Override
	public final void smartComponents(int reqId, Map<Integer, Map.Entry<String, Character>> theMap) {
		logger.info(EWrapperMsgGenerator.smartComponents(reqId, theMap));
	}

	@Override
	public final void tickReqParams(int tickerId, double minTick, String bboExchange, int snapshotPermissions) {
		logger.info(EWrapperMsgGenerator.tickReqParams(tickerId, minTick, bboExchange, snapshotPermissions));
	}

	public final void verifyMessageAPI(String apiData) {
		logger.debug(apiData);
	}

	public final void verifyCompleted(boolean isSuccessful, String errorText) {
		if (!isSuccessful) {
			logger.error(errorText);
		}
	}

	public final void verifyAndAuthMessageAPI(String apiData, String xyzChallenge) {
		logger.debug(apiData + ". Challenge: " + xyzChallenge);
	}

	public final void verifyAndAuthCompleted(boolean isSuccessful, String errorText) {
		if (!isSuccessful) {
			logger.error(errorText);
		}
	}

	public final void marketDataType(int reqId, int marketDataType) {
		logger.info(EWrapperMsgGenerator.marketDataType(reqId, marketDataType));
	}

	public final void commissionReport(CommissionReport commissionReport) {
		logger.info(EWrapperMsgGenerator.commissionReport(commissionReport));
	}

	public final void positionMulti(int reqId, String account, String modelCode, Contract contract, double pos, double avgCost) {
		logger.info(EWrapperMsgGenerator.positionMulti(reqId, account, modelCode, contract, pos, avgCost));
	}

	public final void positionMultiEnd(int reqId) {
		logger.info(EWrapperMsgGenerator.positionMultiEnd(reqId));
	}

	public final void accountUpdateMulti(int reqId, String account, String modelCode, String key, String value, String currency) {
		logger.info(EWrapperMsgGenerator.accountUpdateMulti(reqId, account, modelCode, key, value, currency));
	}

	public final void accountUpdateMultiEnd(int reqId) {
		logger.info(EWrapperMsgGenerator.accountUpdateMultiEnd(reqId));
	}

	public final void receiveFA(int faDataType, String xml) {
		logger.info("Received financial advisor type {} XML : {}", faDataType, xml);
	}

	public final void currentTime(long time) {
		logger.info(EWrapperMsgGenerator.currentTime(time));
	}

	public final void fundamentalData(int reqId, String data) {
		logger.info(EWrapperMsgGenerator.fundamentalData(reqId, data));
	}

	public final void deltaNeutralValidation(int reqId, DeltaNeutralContract deltaNeutralContract) {
		logger.info(EWrapperMsgGenerator.deltaNeutralValidation(reqId, deltaNeutralContract));
	}

	public final void scannerParameters(String xml) {
		logger.debug("Received scanner parameters XML {}", xml);
	}

	public final void error(String str) {
		logger.error(EWrapperMsgGenerator.error(str));
	}

	public final void displayGroupList(int reqId, String groups) {
		logger.info("Group list (request ID: {}): {}}", reqId, groups);
	}

	public final void displayGroupUpdated(int reqId, String contractInfo) {
		logger.info("Group updated (request ID: {}): {}}", reqId, contractInfo);
	}


	public final void tickPrice(int tickerId, int field, double price, TickAttrib attribs) {
		logger.info(EWrapperMsgGenerator.tickPrice(tickerId, field, price, attribs));
	}

	public final void tickOptionComputation(int tickerId, int field, double impliedVol, double delta, double optPrice, double pvDividend, double gamma, double vega, double theta, double undPrice) {
		// received computation tick
		logger.info(EWrapperMsgGenerator.tickOptionComputation(tickerId, field, impliedVol, delta, optPrice, pvDividend, gamma, vega, theta, undPrice));
	}

	public final void tickSize(int tickerId, int field, int size) {
		// received size tick
		logger.info(EWrapperMsgGenerator.tickSize(tickerId, field, size));
	}

	public final void tickGeneric(int tickerId, int tickType, double value) {
		// received generic tick
		logger.info(EWrapperMsgGenerator.tickGeneric(tickerId, tickType, value));
	}

	public final void tickString(int tickerId, int tickType, String value) {
		// received String tick
		logger.info(EWrapperMsgGenerator.tickString(tickerId, tickType, value));
	}

	public final void tickSnapshotEnd(int tickerId) {
		logger.info(EWrapperMsgGenerator.tickSnapshotEnd(tickerId));
	}

	public final void tickEFP(int tickerId, int tickType, double basisPoints, String formattedBasisPoints, double impliedFuture, int holdDays, String futureLastTradeDate, double dividendImpact, double dividendsToLastTradeDate) {
		// received EFP tick
		logger.info(EWrapperMsgGenerator.tickEFP(tickerId, tickType, basisPoints, formattedBasisPoints, impliedFuture, holdDays, futureLastTradeDate, dividendImpact, dividendsToLastTradeDate));
	}

	public final void openOrder(int orderId, Contract contract, Order order, OrderState orderState) {
		// received open order
		logger.info(EWrapperMsgGenerator.openOrder(orderId, contract, order, orderState));
	}

	public final void openOrderEnd() {
		// received open order end
		logger.info(EWrapperMsgGenerator.openOrderEnd());
	}

	public final void scannerData(int reqId, int rank, ContractDetails contractDetails, String distance, String benchmark, String projection, String legsStr) {
		logger.info(EWrapperMsgGenerator.scannerData(reqId, rank, contractDetails, distance,benchmark, projection, legsStr));
	}

	public final void scannerDataEnd(int reqId) {
		logger.info(EWrapperMsgGenerator.scannerDataEnd(reqId));
	}

	public final void bondContractDetails(int reqId, ContractDetails contractDetails) {
		logger.info(EWrapperMsgGenerator.bondContractDetails(reqId, contractDetails));
	}

	public final void execDetails(int reqId, Contract contract, Execution execution) {
		logger.info(EWrapperMsgGenerator.execDetails(reqId, contract, execution));
	}

	public final void execDetailsEnd(int reqId) {
		logger.info(EWrapperMsgGenerator.execDetailsEnd(reqId));
	}

	public final void updateAccountTime(String timeStamp) {
		logger.info("Account time: {}", timeStamp);
	}

	public final void accountDownloadEnd(String accountName) {
		logger.info(EWrapperMsgGenerator.accountDownloadEnd(accountName));
	}

	public final void connectionClosed() {
		logger.info(EWrapperMsgGenerator.connectionClosed());
	}

	@Override
	public final void managedAccounts(String accountsList) {
		logger.info(EWrapperMsgGenerator.managedAccounts(accountsList));
	}

	@Override
	public final void updateNewsBulletin(int msgId, int msgType, String message, String origExchange) {
		logger.info(EWrapperMsgGenerator.updateNewsBulletin(msgId, msgType, message, origExchange));
	}
}
